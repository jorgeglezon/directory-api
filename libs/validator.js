var Validator = require('jsonschema').Validator;

var userSchema = require('../schemas/user.json');
var _v = new Validator();

exports.user = function(json){
    return _v.validate(json, userSchema);
}



exports.v = _v.validate;
