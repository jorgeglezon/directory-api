const config 	= require('../config/config').values;
const logger 	= require('./logger').logger;
const mongodb = require('mongodb');
const cfenv   = require('cfenv');

const MongoClient = mongodb.MongoClient;


var uri = "mongodb://";
if(config.mongo.user != '' && config.mongo.password != '')
uri += config.mongo.user + ":" + config.mongo.password + "@";
uri += config.mongo.host + ":" + config.mongo.port;
var options = {useNewUrlParser: true};
exports.connect = function(callback){
    logger.info('>>>', "conecting to: " + uri);
    MongoClient.connect(uri, options, function(err, database) {
            if(err){ logger.info('>>>', "error: " + err); throw err };
            callback(database.db("directory"));
        }
    );
};
