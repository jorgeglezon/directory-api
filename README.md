# Directory API

## Dependencies

In order to run this project, you need to install the following dependencies:

#
### Node JS and npm

On mac you can simply run:

```bash
brew install node.js
```
#### OR

[Install Node.js](https://nodejs.org/en/download/)

### Installing tools and dependencies


Then install all the necessary libraries in main  proyect

```bash
npm install
```

#
## Install Mongo 
On mac you can simply run:

```bash
brew install mongodb
```

#### OR
[Install Mongo](https://www.mongodb.com/download-center)

#
And run :  

```bash
node app.js
```

## Important
* node versión LTS 
* mongo > 3.0