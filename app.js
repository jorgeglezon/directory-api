const express = require('express');
const bodyParser = require('body-parser');
const app = express();
var config = require('./config/config').values;
const logger = require('./libs/logger').logger;
const mongo = require('./libs/mongo');

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json());
app.set('port', 3000);

var router = express.Router();

router.use(function(req, res, next){
	logger.verbose('>', req.method, req._parsedUrl.pathname);
	logger.debug('<<', req.query, req.body);
	next();
});

app.use('/api', router);
app.use(express.static(__dirname + '/public'));

mongo.connect(function(db){
	logger.info('>>>', "connected to db: " + db.databaseName);
	var test = require('./resources/test').init();

	router.get('/', test.status);
	router.get('/test', test.log);

	var dir = require('./resources/directory').init(db);
    router.get('/directory', dir.getUsers);
    router.get('/directory/:id', dir.getUsersbyId);
		router.post('/directory', dir.newUser);
		router.put('/directory/:id', dir.updateUser);
		router.delete('/directory/:id', dir.deleteUser);

	app.listen(app.get('port'), () => {
		logger.info('>>>', 'listening on ' + config.url + '...');
	});
});
