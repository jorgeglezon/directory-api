const config 	    = require('../config/config').values;
const status 	    = require('../config/status').codes;
const error 	    = require('../libs/error');
const logger 	    = require('../libs/logger').logger;
const validator   = require('../libs/validator');
const mongodb = require('mongodb');
//var crypto      = require('crypto');

var mgi = exports;
var _db, _c;

exports.init = function(db){
    _db = db;
    _c = db.collection('users');
	return mgi;
};

exports.newUser = function(req, res){
    var data = req.body;
    var validation = validator.user(data)
    if(!validation.valid) {
        logger.verbose("invalid input");
		error.input(res, validation.errors);
        return;
    }
    _c.insertOne(data, function(err, result) {
        if (err) return console.log(err)
        console.log('saved to database')
        res.status(status.CREATED).send({"success": true});
    })
}
exports.getUsers = function(req, res) {
    _c.find().toArray((err, result) => {
        if(err){ console.log(err);return; }
        res.status(status.OK).send(result);
    });
}
exports.getUsersbyId = function(req, res, callback) {
    if(mongodb.ObjectID.isValid(req.params.id)) {
        let _id = new mongodb.ObjectID(req.params.id);
        let criteria = {"_id" : _id};
        _c.find(criteria).toArray((err, result) => {
            if(err){ console.log(err);return; }
            res.statusCode = status.OK;
            res.json(result);
        });
    } else {
        logger.verbose("invalid id");
        res.statusCode = status.UNPROCESSABLE
        res.json({error: 'invalid id'});
    }
}

exports.updateUser = function(req, res, callback) {
    if(mongodb.ObjectID.isValid(req.params.id)) {
        let _id = new mongodb.ObjectID(req.params.id);
        let criteria = {"_id" : _id};
        let updateData = {
            $set : {"receiver" : data, "updated" : new Date()}
        }
            _c.update(criteria, updateData, function(err, result){
            if(err){ console.log(err);return; }
            res.statusCode = status.OK;
            res.json(result);
        });
    } else {
        logger.verbose("invalid id");
        res.statusCode = status.UNPROCESSABLE
        res.json({error: 'invalid id'});
    }
}

exports.deleteUser = function(req, res, callback) {
    if(mongodb.ObjectID.isValid(req.params.id)) {
        let _id = new mongodb.ObjectID(req.params.id);
        let criteria = {"_id" : _id};
        _c.remove(criteria).toArray((err, result) => {
            if(err){ console.log(err);return; }
            res.statusCode = status.OK;
            res.json(result);
        });
    } else {
        logger.verbose("invalid id");
        res.statusCode = status.UNPROCESSABLE
        res.json({error: 'invalid id'});
    }
}
