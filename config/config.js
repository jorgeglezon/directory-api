exports.values = {
    debug : true,
	port : 8081,
	url: 'http://localhost:8081',
	log : {
		console_level : 'silly', 		// silly, debug, verbose, info, warn, error
		file_level : 'info',		// info, warn, error
		file : 'errors.log'
	},
    mongo 		: {
		host 	: 'localhost',
		user 	: '',
		pass 	: '',
		port 	: 27017,
		name 	: 'directory'
	},
    otpLength : 6
}
