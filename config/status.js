exports.codes = {
	OK             : 200,
	CREATED        : 201,
	ACCEPTED       : 202,
	DELETED        : 204,
	NOT_MODIFIED   : 304,
	BAD_REQUEST    : 400,
	UNAUTHORIZED   : 401,
	FORBIDDEN      : 403,
	NOT_FOUND      : 404,
	UNPROCESSABLE  : 422
}
